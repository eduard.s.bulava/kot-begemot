import './styles/biblio.css';
import amplitude from 'amplitudejs/dist/amplitude.min'

$(".remove-from-shelf").click(function (e) {
    $(e.target).parent().remove()
    localStorage.removeItem(e.target.getAttribute('data-content'))
})

const bookId = document.getElementById('flat-black-player-container').getAttribute('data-content')





// let progress = JSON.parse(localStorage.getItem(_elements.audio.getAttribute('data-content') + "_progress"))
// if (progress && _currentTrack) {
//     if (_currentTrack === progress.track) {
//         _elements.audio.currentTime = progress.time
//     }
//     $("#total-progress").text(parseInt(progress.total_progress/fullDuration * 100) + "%")
// }


window.onkeydown = function(e) {
    return !(e.keyCode == 32);
};

/*
  Handles a click on the down button to slide down the playlist.
*/
document.getElementsByClassName('down-header')[0].addEventListener('click', function(){
    var list = document.getElementById('list');

    list.style.height = ( parseInt( document.getElementById('flat-black-player-container').offsetHeight ) - 135 ) + 'px';

    document.getElementById('list-screen').classList.remove('slide-out-top');
    document.getElementById('list-screen').classList.add('slide-in-top');
    document.getElementById('list-screen').style.display = "block";
});

/*
  Handles a click on the up arrow to hide the list screen.
*/
document.getElementsByClassName('hide-playlist')[0].addEventListener('click', function(){
    document.getElementById('list-screen').classList.remove('slide-in-top');
    document.getElementById('list-screen').classList.add('slide-out-top');
    document.getElementById('list-screen').style.display = "none";
});

/*
  Handles a click on the song played progress bar.
*/
document.getElementById('song-played-progress').addEventListener('click', function( e ){
    var offset = this.getBoundingClientRect();
    var x = e.pageX - offset.left;

    amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
});

document.querySelector('img[data-amplitude-song-info="cover_art_url"]').style.height = document.querySelector('img[data-amplitude-song-info="cover_art_url"]').offsetWidth + 'px';


$.get("/playlist/" + bookId, function (data) {
    let songs = []
    var durations = []

    var fullDuration = 0

    data.playlist.forEach(function (el) {
        fullDuration += parseInt(el.duration)
        durations.push(parseInt(el.duration))
        songs.push({
            "name": el.title,
            "url": el.mp3,
            "cover_art_url": el.poster
        })
    })

    var startTrack = 0
    if (localStorage.getItem(bookId)) {
        startTrack = parseInt(localStorage.getItem(bookId))
    }


    amplitude.init({
        "bindings": {
            37: 'prev',
            39: 'next',
            32: 'play_pause'
        },
        "songs": songs,
        start_song: startTrack,
        callbacks: {
            play: function () {
                localStorage.setItem(bookId, amplitude.getActiveIndex())
            },
            timeupdate: function () {
                let progress = 0
                durations.every(function (d, i) {
                    if (i === parseInt(amplitude.getActiveIndex())) {
                        progress += parseInt(amplitude.getSongPlayedSeconds())

                        return false
                    }
                    progress += parseInt(d)

                    return true
                })

                console.log(progress,fullDuration,progress/fullDuration)



                localStorage.setItem(bookId + "_progress", JSON.stringify({"track": amplitude.getActiveIndex(), "time": parseInt(amplitude.getSongPlayedSeconds()), "total_progress": progress}))
                $("#total-progress").text("Общий прогресс: " + parseInt(progress/fullDuration * 100) + "%")
            },
            initialized: function () {
                let progress = JSON.parse(localStorage.getItem(bookId + "_progress"))
                if (progress) {
                    if (progress.track) {
                        amplitude.skipTo(progress.time, progress.track)
                    }
                    $("#total-progress").text("Общий прогресс: " + parseInt(progress/fullDuration * 100) + "%")
                }
            }
        }
    });

})






