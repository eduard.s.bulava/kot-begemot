import './wgt';
import './styles/biblio.css';
require('jquery-ui/ui/widgets/draggable');
require('jquery-ui-touch-punch');


var _trackTime = function(seconds) {
    var min = 0;
    var sec = Math.floor(seconds);
    var time = 0;

    min = Math.floor(sec / 60);

    min = min >= 10 ? min : '0' + min;

    sec = Math.floor(sec % 60);

    sec = sec >= 10 ? sec : '0' + sec;

    time = min + ':' + sec;

    return time;
};

var audioPlayer = function() {
    "use strict";
    var _elements = {
        playerButtons: {
            largeToggleBtn: document.querySelector(".large-toggle-btn"),
            nextTrackBtn: document.querySelector(".next-track-btn"),
            previousTrackBtn: document.querySelector(".previous-track-btn"),
            smallToggleBtn: document.getElementsByClassName("small-toggle-btn")
        },
        progressBar: document.querySelector("#audio-progress"),
        playListRows: document.getElementsByClassName("play-list-row"),
        trackInfoBox: document.querySelector(".track-info-box")
    };

    var _progressBarIndicator = document.querySelector("#draggable-point")

    var params = JSON.parse($("#player").attr('data'))
    var wgt = new Biblio()
    wgt.init(params)

    var purchaseIsNeed = wgt.getBookPurchaseIsNeed()
    var playlist = wgt.getTracksMeta()

    var duration = null
    var book = wgt.getBookMeta()
    var fullDuration = book.duration
    let progress = JSON.parse(localStorage.getItem(params.inner_book_id + "_progress"))


    var currentTrackIndex = parseInt(localStorage.getItem(params.inner_book_id)) ? parseInt(localStorage.getItem(params.inner_book_id)) : 0
    var currentTrack = playlist[currentTrackIndex]

    if (progress && currentTrackIndex) {
        $("#total-progress").text(parseInt(progress.total_progress/fullDuration * 100) + "%")
    }

    if (params.user_key) {
        if (purchaseIsNeed) {
            $('#purchase').html('Слушать полностью за ' + wgt.getAmount() + ' руб.').click(function () {
                wgt.startPurchaseBook()
            })
        } else {
            $('#purchase').remove()
        }
    }
    if (purchaseIsNeed) {
        $(".play-list").append(                '                    <div class="play-list-row" data-track-row='+ playlist[0].id +'>\n' +
            '                        <div class="small-toggle-btn">\n' +
            '                            <i class="small-play-btn"><span class="screen-reader-text">Small toggle button</span></i>\n' +
            '                        </div>\n' +
            '                        <div class="track-number">\n' +
            '                            '+ 1 +'.\n' +
            '                        </div>\n' +
            '                        <div class="track-title">\n' +
            '                            <small>' + book.title + '</small>\n' +
            '                        </div>\n' +
            '                    </div>')
    } else {
        playlist.forEach(function (track, i) {
            i += 1
            $(".play-list").append(                '                    <div class="play-list-row" data-track-row='+ track.id +'>\n' +
                '                        <div class="small-toggle-btn">\n' +
                '                            <i class="small-play-btn"><span class="screen-reader-text">Small toggle button</span></i>\n' +
                '                        </div>\n' +
                '                        <div class="track-number">\n' +
                '                            '+i +'.\n' +
                '                        </div>\n' +
                '                        <div class="track-title">\n' +
                '                            <small>' + track.name + '</small>\n' +
                '                        </div>\n' +
                '                    </div>')
        })
        if (currentTrackIndex !== null) {
            updateCurrentTrackInfo(playlist[currentTrackIndex], true)
        }
    }

    $('.play-list-row').click(function (e) {
        var ctrack = parseInt($(this).attr('data-track-row'))
        resetPlayStatus()

        playlist.forEach(function (track, i) {
            if (track.id === ctrack) {
                currentTrack = track
                currentTrackIndex = i
                updateCurrentTrackInfo(playlist[currentTrackIndex])
            }
        })
    })

    function updateCurrentTrackInfo(currentTrack, initial = false) {
        localStorage.setItem(params.inner_book_id, currentTrackIndex)


        $(".track-info-box").css('visibility', 'visible')

        if (purchaseIsNeed) {
            $("#track-title-text").html(book.title)
        } else {
            $("#track-title-text").html(currentTrack.name)
        }

        if (currentTrackIndex === 0) {
            _elements.playerButtons.previousTrackBtn.disabled = true;
            _elements.playerButtons.previousTrackBtn.className = "previous-track-btn disabled";
        } else if (currentTrackIndex > 0 && currentTrackIndex !== playlist.length) {
            _elements.playerButtons.previousTrackBtn.disabled = false;
            _elements.playerButtons.previousTrackBtn.className = "previous-track-btn";
            _elements.playerButtons.nextTrackBtn.disabled = false;
            _elements.playerButtons.nextTrackBtn.className = "next-track-btn";
        } else if (currentTrackIndex + 1 === playlist.length) {
            _elements.playerButtons.nextTrackBtn.disabled = true;
            _elements.playerButtons.nextTrackBtn.className = "next-track-btn disabled";
        }
        resetPlayStatus()
        if (purchaseIsNeed) {
            if ("playing" === wgt.getPlayerState()) {
                wgt.pause()
                _elements.playerButtons.largeToggleBtn.children[0].className = "large-play-btn";
                _elements.playerButtons.smallToggleBtn[currentTrackIndex].children[0].className = "small-play-btn";
            } else {
                wgt.play()
                _elements.playerButtons.largeToggleBtn.children[0].className = "large-pause-btn";
                _elements.playerButtons.smallToggleBtn[currentTrackIndex].children[0].className = "small-pause-btn";
            }
        } else {
            if (!initial) {
                if (wgt.getCurrentTrack() === currentTrack.id ) {
                    if ("playing" === wgt.getPlayerState()) {
                        wgt.pause()
                        _elements.playerButtons.largeToggleBtn.children[0].className = "large-play-btn";
                        _elements.playerButtons.smallToggleBtn[currentTrackIndex].children[0].className = "small-play-btn";
                    } else {
                        wgt.play()
                        _elements.playerButtons.largeToggleBtn.children[0].className = "large-pause-btn";
                        _elements.playerButtons.smallToggleBtn[currentTrackIndex].children[0].className = "small-pause-btn";
                    }
                } else {
                    wgt.play(currentTrack.id)
                    _elements.playerButtons.largeToggleBtn.children[0].className = "large-pause-btn";
                    _elements.playerButtons.smallToggleBtn[currentTrackIndex].children[0].className = "small-pause-btn";
                }
            }
        }

    }

    function resetPlayStatus() {
        var smallToggleBtn = _elements.playerButtons.smallToggleBtn;

        _elements.playerButtons.largeToggleBtn.children[0].className = "large-play-btn";

        for (var i = 0; i < smallToggleBtn.length; i++) {
            if (smallToggleBtn[i].children[0].className === "small-pause-btn") {
                smallToggleBtn[i].children[0].className = "small-play-btn";
            }
        }
    }

    $(".large-play-btn").click(function () {
        if (playlist.length) {
            updateCurrentTrackInfo(playlist[currentTrackIndex]);
        }
    })

    $(".next-track-btn").click(function () {
        if (purchaseIsNeed) {
            return
        }
        if (currentTrackIndex === 0)
            updateCurrentTrackInfo(playlist[currentTrackIndex]);
        if (currentTrackIndex < playlist.length - 1) {
            currentTrackIndex += 1
            updateCurrentTrackInfo(playlist[currentTrackIndex]);
        }
    })

    $(".previous-track-btn").click(function () {
        if (purchaseIsNeed) {
            return
        }
        if (currentTrackIndex === 0)
            updateCurrentTrackInfo(playlist[currentTrackIndex]);
        if (currentTrackIndex > 0) {
            currentTrackIndex -= 1

            updateCurrentTrackInfo(playlist[currentTrackIndex]);
        }
    })

    var isDrag = false

    $('#draggable-point').draggable({
        axis: 'x',
        containment: "#audio-progress"
    });

    $('#draggable-point').draggable({
        start: function() {
            isDrag = true
            wgt.pause()
        },
        stop: function() {
            isDrag = false
            wgt.play()
        },
        drag: function() {
            isDrag = true
            var offset = $(this).offset();
            var xPos = (100 * parseFloat($(this).css("left"))) / (parseFloat($(this).parent().css("width"))) + "%";
            $('#audio-progress-bar').css({
                'width': xPos
            });


            var pos = (100 * parseFloat($(this).css("left"))) / (parseFloat($(this).parent().css("width")))

            var i = Math.round(pos * Math.round(duration ? duration : playlist[currentTrackIndex].duration) / 100);
            if (purchaseIsNeed) {
                wgt._apl.setPosition(i)
            } else {
                wgt.seek(i)
            }
        }
    });


    wgt.on(wgt.EVENT_PROGRESS, function (e) {
        $(".current-time").html(_trackTime(e.position))
        $("span.duration").html(_trackTime(e.duration))

        var indicatorLocation =  (100 * e.position) / (e.duration)

        duration = e.duration
        _progressBarIndicator.style.left = indicatorLocation + "%";

        var progress = 0
        playlist.every(function (d, i) {
            if (i === currentTrackIndex) {
                progress += e.position

                return false
            }
            progress += d.duration

            return true
        })

        localStorage.setItem(params.inner_book_id + "_progress", JSON.stringify({"track": currentTrackIndex, "time": e.position, "total_progress": progress}))

        if (wgt.getBookPurchaseIsNeed()) {
            $("#total-progress").text(parseInt(e.position/fullDuration * 100) + "%")
        } else {
            $("#total-progress").text(parseInt(progress/fullDuration * 100) + "%")
        }

    })

    wgt.on(wgt.EVENT_ENDED, function (e) {
        syncCurrentTrack()
        if (playlist.length > (currentTrackIndex + 1) && !purchaseIsNeed) {
            $(".track-info-box").css('visibility', 'visible')

            if (purchaseIsNeed) {
                $("#track-title-text").html(book.title)
            } else {
                $("#track-title-text").html(currentTrack.name)
            }
            resetPlayStatus()
            _elements.playerButtons.largeToggleBtn.children[0].className = "large-pause-btn";
            _elements.playerButtons.smallToggleBtn[currentTrackIndex].children[0].className = "small-pause-btn";
            localStorage.setItem(params.inner_book_id, currentTrackIndex)

        }
    })

    function syncCurrentTrack() {
        playlist.forEach(function (tr, i) {
            if (tr.id === wgt.getCurrentTrack()) {
                currentTrack = tr
                currentTrackIndex = i
            }
        })

    }
};

(function() {
    var player = new audioPlayer();
})();

