<?php


namespace App\Twig;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class UniqueTextExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('uniqueText', [$this, 'makeUnique']),
        ];
    }

    public function makeUnique($text)
    {
        $sentences = explode('.', $text);

        shuffle($sentences);

        $text = implode('.', $sentences);

        return $text;
    }
}