<?php

namespace App\Controller;


use App\Entity\Book;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\FilterType;
use App\Form\Model\Filter;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CommentController extends AbstractController
{
    /**
     * @Route("/comment/{book}", name="show_comments")
     */
    public function show(Book $book): Response
    {

        return new Response();
    }

    /**
     * @Route("/comment/{book}/create", name="create_comments")
     */
    public function create(Request $request, Book $book): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $comment = new Comment();
        $comment->setUser($user);
        $comment->setAuthorName($user->getName());
        $form = $this->createForm(CommentType::class, $comment, ['book' => $book->getId()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setBook($book);
            $this->getDoctrine()->getManager()->persist($comment);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('show', [
            'slug' => $book->getSlug()
        ]);
    }

}
