<?php

namespace App\Controller;


use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use App\Entity\Part;
use App\Entity\Performer;
use App\Form\CommentType;
use App\Form\FilterType;
use App\Form\Model\Filter;
use Goutte\Client;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class HomepageController extends AbstractController
{

    /**
     * @Route("/", name="landing")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);


        if (array_key_exists('filter', $request->query->all())) {
            $form->handleRequest($request);
        } else {
            $form->submit($request->query->all());
        }

        $response = new Response();
        $response->setMaxAge(600);
        $date = new \DateTime();
        $date->modify('+600 seconds');
        $response->setExpires($date);
        $title = 'Аудиокниги Кот бегемот слушать онлайн бесплатно';

        if ($filter->getAuthor() && ($author = $this->getDoctrine()->getRepository(Author::class)->find($filter->getAuthor()))) {
            $title = $author->getName() . ' слушать онлайн бесплатно';
        }

        return $this->render('homepage/landing.html.twig', [
            'books' => $paginator->paginate($this->getDoctrine()->getRepository(Book::class)->selectByFilter($filter), $request->query->getInt('page', 1), 10),
            'form' => $form->createView(),
            'title' => $title,
            'genres' => $this->getDoctrine()->getRepository(Genre::class)->createQueryBuilder('g')->select('g.name')->groupBy('g.name')->getQuery()->getResult()
        ],
            $response
        );
    }


    /**
     * @Route("/list", name="homepage")
     */
    public function list(Request $request, PaginatorInterface $paginator): Response
    {
        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);


        if (array_key_exists('filter', $request->query->all())) {
            $form->handleRequest($request);
        } else {
            $form->submit($request->query->all());
        }

        $response = new Response();
        $response->setMaxAge(600);
        $date = new \DateTime();
        $date->modify('+600 seconds');
        $response->setExpires($date);
        $title = 'Аудиокниги Кот бегемот слушать онлайн бесплатно';

        if ($filter->getAuthor() && ($author = $this->getDoctrine()->getRepository(Author::class)->find($filter->getAuthor()))) {
            $title = $author->getName() . ' слушать онлайн бесплатно';
        }

        return $this->render('homepage/index.html.twig', [
            'books' => $paginator->paginate($this->getDoctrine()->getRepository(Book::class)->selectByFilter($filter), $request->query->getInt('page', 1), 10),
            'form' => $form->createView(),
            'title' => $title
        ],
            $response
        );
    }

    /**
     * @Route("/book/{slug}", name="show")
     */
    public function show(string $slug): Response
    {
        $book = $this->getDoctrine()->getRepository(Book::class)->findOneBy(['slug' => $slug]);

        if (!$book) {
            throw new NotFoundHttpException();
        }
        $randomByGenre = $this->getDoctrine()->getRepository(Book::class)->findRandomByGenre($book->getGenres(), $book);

        if ($book->isRemoved()) {
            if (count($similar = $this->getDoctrine()->getRepository(Book::class)->findSimilar($book))) {
                $randomByGenre = $similar;
            }
        }
        $form = $this->createForm(CommentType::class, null, ['book' => $book->getId()]);

        if (!$book) {
            throw new NotFoundHttpException();
        }
        $response = new Response();
        $response->setMaxAge(600);
        $date = new \DateTime();
        $date->modify('+600 seconds');
        $response->setExpires($date);
        $user_key = null;

        if ($user = $this->getUser()) {
            $ident_array = [
                "type" => "email",
                "ident" => $user->getEmail()
            ];
            $referal_key = "9eb339092311cda41119d0b69961f0e1dbb3392688e2bc83170c9cf529cc1aa7476420f4c67418f1096c8a71d7a817a8a821";
            $source_string = json_encode($ident_array);
            $ivlen = openssl_cipher_iv_length($cipher = "AES-256-CBC");
            $iv = mb_substr($referal_key, 0, $ivlen);
            $key = mb_substr($referal_key, 10, 35);
            $raw = openssl_encrypt($source_string, $cipher, $key, OPENSSL_RAW_DATA, $iv);
            $hmac = hash_hmac("sha256", $raw, $key, true);
            $user_key = base64_encode($hmac.$raw);
        }

        $template = 'homepage/show.html.twig';

        if ($book->getBiblioId()) {
            $template = 'homepage/show_test.html.twig';
        }

        return $this->render($template, [
            'book' => $book,
            'params' => [
                "book_id"=> $book->getBiblioId(),
                "inner_book_id"=> $book->getId(),
                "referal_key"=> "9eb339092311cda41119d0b69961f0e1dbb3392688e2bc83170c9cf529cc1aa7476420f4c67418f1096c8a71d7a817a8a821",
                "user_key"=> $user_key,
                "selector"=> "#wgt"
            ],
            'user_key' => $user_key,
            'randomBooks' => $randomByGenre,
            'commentForm' => $form->createView(),
            'playlist' => $this->playlist($book)
        ], $response);
    }

    /**
     * @Route("/search", name="search")
     */
    public function search(): Response
    {
        $form = $this->createForm(FilterType::class,null, [
            'action' => $this->generateUrl('homepage'),
        ]);

        return $this->render('homepage/search.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/sitemap.{_format}", name="sample_sitemaps_sitemap1", requirements={"_format" = "xml"})
     */
    public function sitemapAction()
    {
        $urls = [];

        foreach ($this->getDoctrine()->getRepository(Book::class)->createQueryBuilder('b')->select('b.slug')->getQuery()->getResult() as $slug) {
            $slug = $slug['slug'];
            $urls[] = ['loc' => $this->generateUrl('show', ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_URL), 'changefreq' => 'weekly', 'priority' => '1.0'];
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');

        return $this->render('sitemap.xml.twig', [
            'urls' => $urls,
        ]);
    }

    /**
     * @Route("/sitemap2.{_format}", name="sample_sitemaps_sitemap2", requirements={"_format" = "xml"})
     */
    public function sitemap2Action()
    {
        $urls = [];

        foreach ($this->getDoctrine()->getRepository(Book::class)->createQueryBuilder('b')->select('b.slug')->getQuery()->getResult() as $slug) {
            $slug = $slug['slug'];
            $urls[] = ['loc' => $this->generateUrl('show', ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_URL), 'changefreq' => 'daily', 'priority' => '1.0'];
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');

        return $this->render('sitemap.xml.twig', [
            'urls' => $urls,
        ]);
    }

    /**
     * @Route("/sitemap3.{_format}", name="sample_sitemaps_sitemap3", requirements={"_format" = "xml"})
     */
    public function sitemap3Action()
    {
        $urls = [];

        foreach ($this->getDoctrine()->getRepository(Performer::class)->createQueryBuilder('p')->select('p.id')->getQuery()->getResult() as $id) {
            $id = $id['id'];
            $urls[] = ['loc' => $this->generateUrl('homepage', ['performer' => $id], UrlGeneratorInterface::ABSOLUTE_URL), 'changefreq' => 'weekly', 'priority' => '1.0'];
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/xml; charset=utf-8');

        return $this->render('sitemap.xml.twig', [
            'urls' => $urls,
        ]);
    }

    /**
     * @Route("/ads.txt", name="ads")
     */
    public function ads()
    {
        return $this->render('ads.html.twig');
    }

    /**
     * @Route("/shelf", name="shelf")
     */
    public function shelf(Request $request, PaginatorInterface $paginator)
    {
        $books = explode(',', $request->query->get('books'));

        $books = $this->getDoctrine()->getRepository(Book::class)->findBy(['id' => $books]);
        return $this->render('homepage/shelf.html.twig', [
            'books' => $paginator->paginate($books, $request->query->getInt('page', 1), 10),
            'genres' => $this->getDoctrine()->getRepository(Genre::class)->createQueryBuilder('g')->select('g.name')->groupBy('g.name')->getQuery()->getResult()
        ]);
    }

    /**
     * @Route("/copy", name="copy")
     * @Template()
     */
    public function copy()
    {
        return [
        ];
    }


    /**
     * @param Book $book
     * @return array
     */
    public function playlist(Book $book)
    {
        $playlist = [];

        if ($book->getParts()->first()) {
            list($status) = get_headers($book->getParts()->first()->getUrl());

            if (strpos($status, '200') == false) {
                $baseUrl = 'https://bibl.us/';
                $client = new Client();
                $bookUrl = $baseUrl . $book->getSlug() . '.html';
                $crawler2 = $client->request('GET', $bookUrl,        [
                    'headers' => [
                        'Cache-Control' => 'no-cache',
                        'Connection' => 'close'
                    ]
                ], [], [
                    'headers' => [
                        'Cache-Control' => 'no-cache',
                        'Connection' => 'close'
                    ]
                ]);
                try {
                    try {
                        $json = strstr($crawler2->filter('body > script:nth-child(7)')->text(), 'XSPlayer');
                    } catch (\Exception $e) {
                        $json = strstr($crawler2->filter('body > script:nth-child(8)')->text(), 'XSPlayer');

                    }
                    $json = str_replace('XSPlayer(', '', substr($json, 0, strpos($json, ');')));

                    $content = json_decode($json, true);
                    foreach ($book->getParts() as $part) {
                        $this->getDoctrine()->getManager()->remove($part);
                    }
                    $baseUri = 'https://' . $content['mp3_url_prefix'];
                    foreach ($content['tracks'] as $item) {
                        $part = new Part();
                        $part->setBook($book);
                        $book->addPart($part);
                        $part->setDuration($item[2]);
                        $part->setUrl(html_entity_decode($baseUri . '/' . $item[4]));
                        $part->setName(html_entity_decode($item[1]));
                        $this->getDoctrine()->getManager()->persist($part);
                    }
                    $this->getDoctrine()->getManager()->flush();

                } catch (\Exception $exception) {
                }
            }
        }

        foreach ($book->getParts() as $part) {
            $playlist[] = [
                "title" => $part->getName(),
                "mp3" => $part->getUrl() ,
                "poster" => $book->getCover(),
                "duration" => $part->getDuration()
            ];
        }

        return $playlist;
    }

    /**
     * @Route("/playlist/{book}", "playlist")
     */
    public function playlistAction(Book $book) {
        return $this->json([
            'playlist' => $this->playlist($book)
        ]);
    }

    /**
     * @Route("/purchased", name="purchased")
     */
    public function purchased(Request $request, PaginatorInterface $paginator) {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }
        $filter = new Filter();
        $form = $this->createForm(FilterType::class, $filter);


        if (array_key_exists('filter', $request->query->all())) {
            $form->handleRequest($request);
        } else {
            $form->submit($request->query->all());
        }

        $response = new Response();
        $response->setMaxAge(600);
        $date = new \DateTime();
        $date->modify('+600 seconds');
        $response->setExpires($date);
        $title = 'Аудиокниги Кот бегемот слушать онлайн бесплатно';


        $ident_array = [
            "type" => "email",
            "ident" => $this->getUser()->getEmail()
        ];
        $referal_key = "9eb339092311cda41119d0b69961f0e1dbb3392688e2bc83170c9cf529cc1aa7476420f4c67418f1096c8a71d7a817a8a821";
        $source_string = json_encode($ident_array);
        $ivlen = openssl_cipher_iv_length($cipher = "AES-256-CBC");
        $iv = mb_substr($referal_key, 0, $ivlen);
        $key = mb_substr($referal_key, 10, 35);
        $raw = openssl_encrypt($source_string, $cipher, $key, OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac("sha256", $raw, $key, true);
        $user_key = base64_encode($hmac.$raw);

        $client = new \GuzzleHttp\Client();

        $resp = $client->get('https://admin.bibliovk.ru/api/ref/me/rest-data', [
            'headers' => [
                'X-Biblio-Auth' => 'Bearer 9eb339092311cda41119d0b69961f0e1dbb3392688e2bc83170c9cf529cc1aa7476420f4c67418f1096c8a71d7a817a8a821',
                'Content-Type' => 'application/json',
                'X-Biblio-User' => $user_key
            ],

        ]);

        $content = json_decode($resp->getBody(), true);


        if (isset($content['purchased_books'])) {
            $books = $this->getDoctrine()->getRepository(Book::class)->findBy(['biblioId' => $content['purchased_books']]);
        } else {
            $books = $this->getDoctrine()->getRepository(Book::class)->selectByFilter($filter);
        }


        return $this->render('homepage/index.html.twig', [
            'books' => $paginator->paginate($books, $request->query->getInt('page', 1), 10),
            'form' => $form->createView(),
            'title' => $title,
            'genres' => $this->getDoctrine()->getRepository(Genre::class)->createQueryBuilder('g')->select('g.name')->groupBy('g.name')->getQuery()->getResult()
        ],
            $response
        );


    }
}
