<?php

namespace App\Controller;


use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Comment;
use App\Entity\Genre;
use App\Entity\Performer;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\FilterType;
use App\Form\Model\Filter;
use Doctrine\Common\Collections\Criteria;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NavigationController extends AbstractController
{
    /**
     * @Route("/genres", name="genres")
     */
    public function genres(): Response
    {
        $genres = $this->getDoctrine()->getRepository(Genre::class)
            ->createQueryBuilder('g')
            ->orderBy('g.name', Criteria::ASC)
            ->select('g.name')
            ->groupBy('g.name')
            ->getQuery()
            ->getResult();

        return $this->render('navigation/genre.html.twig', [
            'genres' => $genres
        ]);
    }

    /**
     * @Route("/authors", name="authors")
     */
    public function authors(Request $request, PaginatorInterface $paginator): Response
    {
        $authors = $this->getDoctrine()->getRepository(Author::class)
            ->createQueryBuilder('a')
            ->orderBy('a.name', Criteria::ASC)
            ->select('a.name', 'a.id')
            ->groupBy('a.name', 'a.id')
            ->getQuery()
            ->getResult();

        return $this->render('navigation/authors.html.twig', [
            'authors' => $paginator->paginate($authors, $request->query->getInt('page', 1), 100),
        ]);
    }

    /**
     * @Route("/performers", name="performers")
     */
    public function performers(Request $request, PaginatorInterface $paginator): Response
    {
        $performers = $this->getDoctrine()->getRepository(Performer::class)
            ->createQueryBuilder('p')
            ->orderBy('p.name', Criteria::ASC)
            ->select('p.name', 'p.id')
            ->groupBy('p.name', 'p.id')
            ->getQuery()
            ->getResult();

        return $this->render('navigation/performers.html.twig', [
            'performers' => $paginator->paginate($performers, $request->query->getInt('page', 1), 100),
        ]);
    }
}
