<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\Model\RestorePassword;
use App\Form\ResetPasswordType;
use App\Form\RestorePasswordType;
use App\Manager\SendInBlueMailer;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var SendInBlueMailer
     */
    private $sendInBlueMailer;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SendInBlueMailer $sendInBlueMailer)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->sendInBlueMailer = $sendInBlueMailer;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/restore-password", name="restore_password")
     */
    public function restorePassword(Request $request)
    {
        $restorePassword = new RestorePassword();
        $form = $this->createForm(RestorePasswordType::class, $restorePassword);

        if ($request->getMethod() == Request::METHOD_POST) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var User $user */
                if ($user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $restorePassword->getEmail()])) {
                    $user->setResetPasswordToken(base64_encode(random_bytes(8)));
                    $this->getDoctrine()->getManager()->flush();
                    $this->sendInBlueMailer->sendRestoreEmail($user);

                    $this->addFlash("success", "Email отправлен на вашу почту");
                } else {
                    $this->addFlash("danger", "Пользованель не найден");
                }

                return $this->redirectToRoute("homepage");
            }
        }
        return $this->render('security/restore.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password")
     */
    public function resetPassword(Request $request, string $token)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['resetPasswordToken' => $token]);

        $form = $this->createForm(ResetPasswordType::class, $user, [
            'method' => 'post',
            'action' => $this->generateUrl('reset_password', [
                'token' => $token,
            ]),
        ]);

        if ($request->getMethod() == Request::METHOD_POST) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));
                $user->setResetPasswordToken(null);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash("success", "Ваш пароль восстановлен.");

                return $this->redirectToRoute("homepage");
            }
        }

        return $this->render('security/reset.html.twig', [
           'form' => $form->createView()
        ]);

    }


}
