<?php

namespace App\Command;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use App\Entity\Page;
use App\Entity\Part;
use App\Entity\Performer;
use Doctrine\Persistence\ManagerRegistry;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScrapCommand extends Command
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    private $parts = [];

    public function __construct(string $name = null, ManagerRegistry $doctrine)
    {
        parent::__construct($name);
        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'scrap';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('totalPages', InputArgument::OPTIONAL, 'Argument description')
            ->addArgument('url', InputArgument::OPTIONAL, 'url')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $baseUrl = $input->getArgument('url') ?: 'https://bibl.us';
        ini_set('memory_limit', '4048M');
        $client = new Client();
        $lastPage = 1;

        $totalPages = $input->getArgument('totalPages') ?: 1;

        while ($lastPage <= (int) $totalPages) {
                $crawler = $client->request('GET', "$baseUrl/?p={$lastPage}",        [
                    'headers' => [
                        'Cache-Control' => 'no-cache',
                        'Connection' => 'close'
                    ]
                ], [], [
                    'headers' => [
                        'Cache-Control' => 'no-cache',
                        'Connection' => 'close'
                    ]
                ]);
                $this->parts = [];

                $crawler->filter('#books_list')->children()->each(function (Crawler $crawler) {
                    $this->parts[] = $crawler->filter('a')->attr('href');
                });


                foreach ($this->parts as $partUrl) {
                    try {

                    $slug = str_replace('/', '', $partUrl);
                    $slug = str_replace('.html', '', $slug);

                    $book = $this->doctrine->getRepository(Book::class)->findOneBy(['slug' => $slug]) ?: new Book();

                    $book->setSlug($slug);
                    $bookUrl = $baseUrl . $partUrl;
                    $crawler2 = $client->request('GET', $bookUrl,        [
                        'headers' => [
                            'Cache-Control' => 'no-cache',
                            'Connection' => 'close'
                        ]
                    ], [], [
                        'headers' => [
                            'Cache-Control' => 'no-cache',
                            'Connection' => 'close'
                        ]
                    ]);
                    try {
                        $title = $crawler2->filter('body > div._3148a3 > div > div._0a4964 > div._c10a44')->text();
                        try {
                            $ar = $crawler2->filter('body > div._3148a3 > div:nth-child(2) > div._79758c._f1ae22')->text();
                            if ($ar == 'Аудиокнига удалена по требованию правообладателя.') {
                                continue;
                            }
                        } catch (\Exception $exception) {

                        }

                    } catch (\Exception $exception) {
//                        if ($book->getId()) {
//                            $this->doctrine->getManager()->remove($book);
//                            $this->doctrine->getManager()->flush();
//                        }
                        continue;
                    }
                    $book->setTitle(trim($title));

                    $description = $crawler2->filter('body > div._3148a3 > div > div._6f05d7._5c0f1a > span')->text();

                    $book->setDescription(trim($description));

                    if (!$book->getId()) {
                        $crawler2->filter('body > div._3148a3 > div > div._72ce76')->children()->each(function (Crawler $crawler) use ($book) {
                            switch ($crawler->filter('b')->text()) {
                                case 'Жанр':
                                    $genres = explode(',', $crawler->filter('a')->text());

                                    foreach ($genres as $genre) {
                                        $genre = trim($genre);

                                        if (!$genreEntity = $this->doctrine->getRepository(Genre::class)->findOneBy(['name' => $genre])) {
                                            $genreEntity = new Genre();
                                            $genreEntity->setName($genre);
                                            $this->doctrine->getManager()->persist($genreEntity);
                                        }
                                        $genreEntity->addBook($book);
                                        $book->addGenre($genreEntity);
                                    }
                                    break;
                                case 'Читают:':
                                    $crawler->children()->each(function (Crawler $crawler) use ($book) {
                                        $crawler->filter('a')->each(function (Crawler $crawler) use ($book) {
                                            $performer = $crawler->text();
                                            if (!$performerEntity = $this->doctrine->getRepository(Performer::class)->findOneBy(['name' => $performer])) {
                                                $performerEntity = new Performer();
                                                $performerEntity->setName($performer);
                                                $this->doctrine->getManager()->persist($performerEntity);
                                            }

                                            $book->addPerformer($performerEntity);
                                        });
                                    });
                                    break;
                                case 'Читает:':
                                    $performer = $crawler->filter('a')->text();
                                    if (!$performerEntity = $this->doctrine->getRepository(Performer::class)->findOneBy(['name' => $performer])) {
                                        $performerEntity = new Performer();
                                        $performerEntity->setName($performer);
                                        $this->doctrine->getManager()->persist($performerEntity);
                                    }

                                    $book->addPerformer($performerEntity);
                                    break;
                                case 'Длительность:':
                                    $duration = trim($crawler->text());
                                    $book->setDuration(trim($duration));
                                    break;
                                case 'Автор:':
                                    $author = $crawler->filter('span > a')->text();

                                    $author = trim($author);

                                    if (!$authorEntity = $this->doctrine->getRepository(Author::class)->findOneBy(['name' => $author])) {
                                        $authorEntity = new Author();
                                        $authorEntity->setName($author);
                                        $this->doctrine->getManager()->persist($authorEntity);
                                    }

                                    $book->addAuthor($authorEntity);
                                    break;
                                case 'Авторы:':
                                    $crawler->children()->each(function (Crawler $crawler) use ($book) {
                                        $crawler->filter('span > a')->each(function (Crawler $crawler) use ($book) {
                                            $author = $crawler->text();
                                            if (!$authorEntity = $this->doctrine->getRepository(Author::class)->findOneBy(['name' => $author])) {
                                                $authorEntity = new Author();
                                                $authorEntity->setName($author);
                                                $this->doctrine->getManager()->persist($authorEntity);
                                            }

                                            $book->addAuthor($authorEntity);
                                        });
                                    });
                                    break;
                            }

                        });


                        try {
                            $cover = trim($crawler2->filter('body > div:nth-child(5) > div:nth-child(2) > div:nth-child(5) > div > img')->attr('src'));
                        } catch (\Exception $exception) {
                            $cover = trim($crawler2->filter('body > div._3148a3 > div > div._6f05d7._5c0f1a > div > img')->attr('src'));
                        }
                        $book->setCover($cover);
                    }
                        try {
                            try {
                                $json = strstr($crawler2->filter('body > script:nth-child(7)')->text(), 'XSPlayer');
                            } catch (\Exception $e) {
                                $json = strstr($crawler2->filter('body > script:nth-child(8)')->text(), 'XSPlayer');

                            }
                            $json = str_replace('XSPlayer(', '', substr($json, 0, strpos($json, ');')));

                            $content = json_decode($json, true);

                            if (isset($content['biblio']) && isset($content['biblio']['book_id'])) {
                                $book->setBiblioId((int) $content['biblio']['book_id']);
                            }

                            if (!$book->getId() || $book->getParts()->isEmpty() || ($book->getParts()->count() == 1 && $book->getParts()->first()->getName() == "Вступление")) {
                                try {
                                    $baseUri = 'https://' . $content['mp3_url_prefix'];
                                    foreach ($content['tracks'] as $item) {
                                        $part = new Part();
                                        $part->setBook($book);
                                        $part->setDuration($item[2]);
                                        $part->setUrl(html_entity_decode($baseUri . '/' . $item[4]));
                                        $part->setName(html_entity_decode($item[1]));
                                        $this->doctrine->getManager()->persist($part);
                                    }
                                } catch (\Exception $e) {
                                }
                            }
                        } catch (\Exception $exception) {
                            echo $exception->getMessage();
                            echo 3;
                            continue;
                        }

                    $this->doctrine->getManager()->persist($book);
                    } catch (\Exception $e) {
                        echo $partUrl ;
                        echo "\n";
                        echo $e->getMessage();
                        echo "\n";
                        echo $e->getLine();
                        echo "\n";

                    }
                }
                $lastPage += 1;
                $this->doctrine->getManager()->flush();
        }

        return Command::SUCCESS;
    }
}
