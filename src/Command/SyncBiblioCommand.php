<?php

namespace App\Command;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use App\Entity\Page;
use App\Entity\Part;
use App\Entity\Performer;
use Doctrine\Persistence\ManagerRegistry;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SyncBiblioCommand extends Command
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    private $parts = [];

    public function __construct(string $name = null, ManagerRegistry $doctrine)
    {
        parent::__construct($name);
        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'syncBiblio';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $url = 'https://admin.bibliovk.ru/api/ref/data/catalog/full';
        ini_set('memory_limit', '4048M');
        $client = new \GuzzleHttp\Client();

        $perPage = 100;
        $currentPage = 1;
        $totalPages = 1000;

        while ($currentPage <= (int) $totalPages) {


            $response = $client->get($url . "?per_page=$perPage&page=$currentPage", [
                'headers' => [
                    'X-Biblio-Auth' => 'Bearer 9eb339092311cda41119d0b69961f0e1dbb3392688e2bc83170c9cf529cc1aa7476420f4c67418f1096c8a71d7a817a8a821',
                    'Content-Type' => 'application/json'
                ]
            ]);

            $response = json_decode($response->getBody()->getContents(), true);

            $currentPage = $response['current_page'] + 1;
            $totalPages = $response['last_page'];

            foreach ($response['data'] as $biblioBook) {
                $biblioAuthor = explode(' ', $biblioBook['author_name']);
                $biblioperformer = explode(' ', $biblioBook['reader_name']);

                $books = $this->doctrine->getRepository(Book::class)->findBy(['title' => $biblioBook['title']]);
                $matched = false;
                /** @var Book $book */
                foreach ($books as $book) {
                    $authorMatch = false;
                    $performerMatch = false;
                    foreach ($book->getAuthors() as $author) {
                        if (false !== strpos($author->getName(), $biblioAuthor[0])) {
                            $authorMatch = true;
                            break;
                        }
                    }

                    foreach ($book->getPerformer() as $performer) {
                        if (false !== strpos($performer->getName(), $biblioperformer[0])) {
                            $performerMatch = true;
                            break;
                        }
                    }

                    if ($authorMatch && $book->getParts()->count() != $biblioBook['tracks_countv']) {
                        $book->setRemoved(true);
                    }

                    if ($authorMatch && ($book->getParts()->count() == $biblioBook['tracks_countv'] || $performerMatch)) {
                        $book->setBiblioId($biblioBook['id']);
                        $book->setRemoved(false);
                        $this->doctrine->getManager()->flush();
                        $matched = true;
                    }
                }

                if (!$matched) {
                    $bebraBook = new Book();
                    $this->doctrine->getManager()->persist($bebraBook);

                    $hours = (int) ($biblioBook['duration'] / 60 / 60);

                    $minutes = (int) ($biblioBook['duration'] / 60) - $hours * 60;
                    $duration = "Длительность: $hours ч. $minutes мин.";


                    $bebraBook
                        ->setCover($biblioBook['cover'])
                        ->setDuration($duration)
                        ->setSlug($biblioBook['id'])
                        ->setTitle($biblioBook['title'])
                        ->setDescription($biblioBook['bio'])
                        ->setBiblioId($biblioBook['id']);

                    foreach (explode(',', $biblioBook['author_name']) as $bebraAuthor) {
                        $bebraAuthorName = implode(' ', array_reverse(explode(' ', $bebraAuthor)));
                        if ($bebraAuthor = $this->doctrine->getRepository(Author::class)->findOneBy(['name' => $bebraAuthorName])) {
                            $bebraBook->addAuthor($bebraAuthor);
                            $bebraAuthor->addBook($bebraBook);
                        } else {
                            $authorEntity = new Author();
                            $authorEntity->setName($bebraAuthorName);
                            $this->doctrine->getManager()->persist($authorEntity);
                            $bebraBook->addAuthor($authorEntity);
                            $authorEntity->addBook($bebraBook);
                        }
                    }

                    foreach (explode(',', $biblioBook['genres']) as $bebraGenre) {
                        $bebraGenreName = implode(' ', explode(' ', $bebraGenre));
                        if ($bebraGenre = $this->doctrine->getRepository(Genre::class)->findOneBy(['name' => $bebraGenreName])) {
                            $bebraBook->addGenre($bebraGenre);
                            $bebraGenre->addBook($bebraBook);
                        } else {
                            $genreEntity = new Genre();
                            $genreEntity->setName($bebraGenreName);
                            $this->doctrine->getManager()->persist($genreEntity);
                            $bebraBook->addGenre($genreEntity);
                            $genreEntity->addBook($bebraBook);
                        }
                    }

                    foreach (explode(',', $biblioBook['reader_name']) as $bebraPerformer) {
                        $bebraPerformerName = implode(' ', array_reverse(explode(' ', $bebraPerformer)));
                        if ($bebraPerformer = $this->doctrine->getRepository(Performer::class)->findOneBy(['name' => $bebraPerformerName])) {
                            $bebraBook->addPerformer($bebraPerformer);
                            $bebraPerformer->addBook($bebraBook);
                        } else {
                            $performerEntity = new Performer();
                            $performerEntity->setName($bebraPerformerName);
                            $this->doctrine->getManager()->persist($performerEntity);
                            $bebraBook->addPerformer($performerEntity);
                            $performerEntity->addBook($bebraBook);
                        }
                    }

                    $part = new Part();
                    $part->setUrl($biblioBook['audio_sample'])
                        ->setName($bebraBook->getTitle())
                        ->setBook($bebraBook)
                        ->setDuration(1);
                    $this->doctrine->getManager()->persist($part);
                    $bebraBook->addPart($part);
                }
                $this->doctrine->getManager()->flush();
            }
        }

        return Command::SUCCESS;
    }
}
