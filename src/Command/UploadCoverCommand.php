<?php

namespace App\Command;

use App\Entity\Book;
use App\Manager\CloudStorageManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadCoverCommand extends Command
{
    /**
     * @var CloudStorageManager
     */
    private $cloudStorageManager;
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(string $name = null, CloudStorageManager $cloudStorageManager, ManagerRegistry $doctrine)
    {
        parent::__construct($name);

        $this->cloudStorageManager = $cloudStorageManager;
        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'uploadCover';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $batchSize = 5;
        $i = 1;
        $em = $this->doctrine->getManager();
        /** @var Book $book */
        while ($book = $this->doctrine->getRepository(Book::class)->createQueryBuilder('b')->andWhere("b.gcpCover IS NULL")->getQuery()->setMaxResults(1)->getOneOrNullResult()) {
            if ($book->getGcpCover() !== null || $book->getGcpCover() === '1') {
                continue;
            }
            $book->setGcpCover('1');
            $em->flush();
            $this->cloudStorageManager->uploadBookCover($book);
            if (($i % $batchSize) === 0) {
                $em->flush();
                $em->clear();
            }
            ++$i;
        }
        $em->flush();

        return Command::SUCCESS;
    }
}
