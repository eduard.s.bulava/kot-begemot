<?php

namespace App\Command;

use App\Entity\Part;
use App\Manager\CloudStorageManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadPartCommand extends Command
{
    /**
     * @var CloudStorageManager
     */
    private $cloudStorageManager;
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(string $name = null, CloudStorageManager $cloudStorageManager, ManagerRegistry $doctrine)
    {
        parent::__construct($name);

        $this->cloudStorageManager = $cloudStorageManager;
        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'uploadPart';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $batchSize = 5;
        $i = 1;
        $em = $this->doctrine->getManager();
        /** @var Part $part */
        while ($part = $this->doctrine->getRepository(Part::class)->createQueryBuilder('p')->andWhere("p.gcpUrl IS NULL")->getQuery()->setMaxResults(1)->getOneOrNullResult()) {
            if ($part->getGcpUrl() !== null || $part->getGcpUrl() === '1') {
                continue;
            }
            $part->setGcpUrl('1');
            $em->flush();
            $this->cloudStorageManager->uploadPart($part);
            if (($i % $batchSize) === 0) {
                $em->flush();
                $em->clear();
            }
            ++$i;
        }
        $em->flush();

        return Command::SUCCESS;
    }
}
