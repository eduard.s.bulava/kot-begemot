<?php

namespace App\Command;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use App\Entity\Page;
use App\Entity\Part;
use App\Entity\Performer;
use Doctrine\Persistence\ManagerRegistry;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ScrapPartCommand extends Command
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(string $name = null, ManagerRegistry $doctrine)
    {
        parent::__construct($name);
        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'scrapPart';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $batchSize = 20;
        $i = 1;
        $baseUrl = 'https://bibl.us/';
        ini_set('memory_limit', '-1');
        $client = new Client();
        $books = $this->doctrine->getRepository(Book::class)->createQueryBuilder('b')->andWhere('b.biblioId IS NOT NULL')->getQuery()->toIterable();
        /** @var Book $book */
        foreach ($books as $book) {
            if ($book->getParts()->first()) {
                list($status) = get_headers($book->getParts()->first()->getUrl());

                if (strpos($status, '502') === FALSE && strpos($status, '404') === FALSE) {
                    continue;
                }
            }

            $bookUrl = $baseUrl . $book->getSlug() . '.html';
                    $crawler2 = $client->request('GET', $bookUrl,        [
                        'headers' => [
                            'Cache-Control' => 'no-cache',
                            'Connection' => 'close'
                        ]
                    ], [], [
                        'headers' => [
                            'Cache-Control' => 'no-cache',
                            'Connection' => 'close'
                        ]
                    ]);

                        try {
                            $json = strstr($crawler2->filter('body > script:nth-child(7)')->text(), 'XSPlayer');
                            $json = str_replace('XSPlayer(', '', substr($json, 0, strpos($json, ');')));

                            $content = json_decode($json, true);
//                file_put_contents('books/'.$title.'/cover.jpg', file_get_contents($cover));

                                foreach ($book->getParts() as $part) {
                                    $this->doctrine->getManager()->remove($part);
                                }
                                $baseUri = 'https://' . $content['mp3_url_prefix'];
                                foreach ($content['tracks'] as $item) {
                                    $part = new Part();
                                    $part->setBook($book);
                                    $part->setDuration($item[2]);
                                    $part->setUrl(html_entity_decode($baseUri . '/' . $item[4]));
                                    $part->setName(html_entity_decode($item[1]));
                                    $this->doctrine->getManager()->persist($part);
                            }


//                    file_put_contents($title.'/'.$item[4], file_get_contents($baseUri . '/' . $item[4]));

                        } catch (\Exception $exception) {
                            continue;
                        }
            if (($i % $batchSize) === 0) {
                $this->doctrine->getManager()->flush(); // Executes all updates.
                $this->doctrine->getManager()->clear(); // Detaches all objects from Doctrine!
            }
            ++$i;
                        $output->writeln('book:' . $book->getId());
        }
        $this->doctrine->getManager()->flush();


        return Command::SUCCESS;
    }
}
