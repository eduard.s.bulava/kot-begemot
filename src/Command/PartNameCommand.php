<?php

namespace App\Command;

use App\Entity\Part;
use App\Manager\CloudStorageManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PartNameCommand extends Command
{
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(string $name = null, ManagerRegistry $doctrine)
    {
        parent::__construct($name);

        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'partName';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $batchSize = 5;
        $i = 1;
        $q = $this->doctrine->getRepository(Part::class)->createQueryBuilder('p')->getQuery();
        $em = $this->doctrine->getManager();
        foreach ($q->toIterable() as $part) {
            $name = html_entity_decode($part->getName());
            $array = preg_split('- \d+ -', $name);
            if (isset($array[1])) {
                $name = str_replace('- ', '', $array[1]);
            }
            $part->setName($name);
            if (($i % $batchSize) === 0) {
                $em->flush(); // Executes all updates.
                $em->clear(); // Detaches all objects from Doctrine!
            }
            ++$i;
        }
        $em->flush();

        return Command::SUCCESS;
    }
}
