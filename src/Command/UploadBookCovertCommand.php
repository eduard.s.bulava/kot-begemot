<?php

namespace App\Command;

use App\Entity\Book;
use App\Entity\Part;
use App\Manager\CloudStorageManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UploadBookCovertCommand extends Command
{
    /**
     * @var CloudStorageManager
     */
    private $cloudStorageManager;
    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(string $name = null, CloudStorageManager $cloudStorageManager, ManagerRegistry $doctrine)
    {
        parent::__construct($name);

        $this->cloudStorageManager = $cloudStorageManager;
        $this->doctrine = $doctrine;
    }

    protected static $defaultName = 'uploadCover';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $batchSize = 20;
        $i = 1;
        $q = $this->doctrine->getRepository(Book::class)->createQueryBuilder('b')->andWhere("b.gcpCover = ''")->getQuery();
        $em = $this->doctrine->getManager();
        foreach ($q->toIterable() as $book) {
            $this->cloudStorageManager->uploadBookCover($book);
            if (($i % $batchSize) === 0) {
                $em->flush(); // Executes all updates.
                $em->clear(); // Detaches all objects from Doctrine!
            }
            ++$i;
        }
        $em->flush();

        return Command::SUCCESS;
    }
}
