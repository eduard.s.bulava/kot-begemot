<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 *
 * @ORM\Table(
 *     indexes={
 *         @ORM\Index(columns={"slug"})
 *     }
 * )
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gcpCover;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $duration;

    /**
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @var Author[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Author", mappedBy="books")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $authors;

    /**
     * @var Performer[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Performer", mappedBy="book")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $performer;

    /**
     * @var Series
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Series")
     */
    private $series;

    /**
     * @var Genre
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Genre", mappedBy="books")
     */
    private $genres;

    /**
     * @var Part[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Part", mappedBy="book")
     */
    private $parts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="book")
     */
    private $comments;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $removed;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $biblioId;

    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->parts = new ArrayCollection();
        $this->performer = new ArrayCollection();
        $this->authors = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSeries(): ?Series
    {
        return $this->series;
    }

    public function setSeries(?Series $series): self
    {
        $this->series = $series;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->genres->removeElement($genre);

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Part[]
     */
    public function getParts(): Collection
    {
        return $this->parts;
    }

    public function addPart(Part $part): self
    {
        if (!$this->parts->contains($part)) {
            $this->parts[] = $part;
            $part->setBook($this);
        }

        return $this;
    }

    public function removePart(Part $part): self
    {
        if ($this->parts->removeElement($part)) {
            // set the owning side to null (unless already changed)
            if ($part->getBook() === $this) {
                $part->setBook(null);
            }
        }

        return $this;
    }

    public function __toStrng()
    {
        return $this->getSlug();
    }

    /**
     * @return Collection|Performer[]
     */
    public function getPerformer(): Collection
    {
        return $this->performer;
    }

    public function addPerformer(Performer $performer): self
    {
        if (!$this->performer->contains($performer)) {
            $this->performer[] = $performer;
            $performer->addBook($this);
        }

        return $this;
    }

    public function removePerformer(Performer $performer): self
    {
        if ($this->performer->removeElement($performer)) {
            $performer->removeBook($this);
        }

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
            $author->addBook($this);
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->removeElement($author)) {
            $author->removeBook($this);
        }

        return $this;
    }

    public function getGcpCover()
    {
        return $this->gcpCover;
    }

    public function setGcpCover($gcpCover): self
    {
        $this->gcpCover = $gcpCover;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setBook($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getBook() === $this) {
                $comment->setBook(null);
            }
        }

        return $this;
    }

    public function isRemoved():? bool
    {
        return $this->removed;
    }

    public function setRemoved(?bool $removed): void
    {
        $this->removed = $removed;
    }

    /**
     * @return int
     */
    public function getBiblioId():? int
    {
        return $this->biblioId;
    }

    /**
     * @param int $biblioId
     */
    public function setBiblioId(?int $biblioId): self
    {
        $this->biblioId = $biblioId;

        return $this;
    }

}
