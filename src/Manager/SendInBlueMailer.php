<?php

namespace App\Manager;

use App\Entity\User;
use GuzzleHttp\Client;
use Symfony\Component\Routing\RouterInterface;

class SendInBlueMailer
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(
        Client $client,
        RouterInterface $router
    ) {
        $this->client = $client;
        $this->router = $router;
    }


    public function sendRestoreEmail(User $user)
    {
        $this->client->post('smtp/email', [
            'json' => [
                'templateId' => 10,
                'to' => [['email' => $user->getEmail()]],
                'params' => ["LINK" => $this->router->generate('reset_password', ['token' => $user->getResetPasswordToken()], RouterInterface::ABSOLUTE_URL)],
            ],
        ]);
    }
}
