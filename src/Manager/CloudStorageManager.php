<?php

namespace App\Manager;

use App\Entity\Book;
use App\Entity\Part;
use Google\Cloud\Storage\StorageClient;
use GuzzleHttp\Client;

class CloudStorageManager
{
    /**
     * @var StorageClient
     */
    private $client;
    /**
     * @var Client
     */
    private $guzzle;


    public function __construct(StorageClient $client, Client $guzzle)
    {
        $this->client = $client;
        $this->guzzle = $guzzle;
    }

    public function uploadPart(Part $part)
    {
        $bucket = $this->client->bucket('bebra-part');

        $result = $bucket->getStreamableUploader(fopen("{$part->getUrl()}", 'r'),             [
            'predefinedAcl' => 'publicRead',
            'name' => $part->getBook()->getTitle() . '/' . $part->getId() . html_entity_decode($part->getName()) . '.mp3',
            'metadata' => [
                'contentType' => 'audio/mpeg',
            ],
        ])->upload();

        $part->setGcpUrl('https://storage.cloud.google.com/' . $result['bucket'] . '/' . $result['name']);
    }

    public function uploadBookCover(Book $book)
    {
        $bucket = $this->client->bucket('bebra-part');
        $result = $bucket->upload(
            file_get_contents($book->getCover()),
            [
                'predefinedAcl' => 'publicRead',
                'name' => $book->getTitle() . rand(0,10000) . 'title/' . $book->getTitle() . '.jpeg',
                'metadata' => [
                    'contentType' => 'image/jpeg',
                ],
            ]
        );

        $book->setGcpCover('https://storage.cloud.google.com/' . $result->info()['bucket'] . '/' . $result->info()['name']);
    }
}
