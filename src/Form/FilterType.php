<?php

namespace App\Form;

use App\Form\Model\Filter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextType::class, [
                'label' => false,
                'attr' => [
//                    'style' => 'width: 270px',
                    'class' => 'form-control mr-sm-2',
                    'placeholder' => "Найти автора, книгу, исполнителя..."
                ]
            ])
            ->add('author', HiddenType::class)
            ->add('performer', HiddenType::class)
            ->add('genre', HiddenType::class)
//            ->add('Search', SubmitType::class, [
//                'label' => 'Поиск',
//                'attr' => [
//                    'class' => 'btn btn-outline-success',
//                ]]
//            )
        ;
        $builder->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Filter::class,
            'attr' => [
//                'style' => 'display: flex',
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}
