<?php


namespace App\Form\Model;


class Filter
{
    /**
     * @var string
     */
    private $text;

    private $author;
    private $performer;
    private $genre;

    /**
     * @return string
     */
    public function getText():? string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getPerformer()
    {
        return $this->performer;
    }

    /**
     * @param mixed $performer
     */
    public function setPerformer($performer): void
    {
        $this->performer = $performer;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setGenre($genre): void
    {
        $this->genre = $genre;
    }
}