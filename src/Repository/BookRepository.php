<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Genre;
use App\Form\Model\Filter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function selectByFilter(Filter $filter) {
        $qb = $this->createQueryBuilder('b');

        if ($filter->getText()) {
            $qb
                ->join('b.authors', 'authors')
                ->join('b.performer', 'performer')
                ->andWhere('b.title LIKE :text')
                ->orWhere('authors.name LIKE :text')
                ->orWhere('performer.name LIKE :text')
                ->setParameter('text', '%' . $filter->getText() . '%');
        }

        if ($filter->getAuthor()) {
            $qb
                ->join('b.authors', 'a')
                ->andWhere('a.id = :author')->setParameter('author', $filter->getAuthor());
        }

        if ($filter->getPerformer()) {
            $qb
                ->join('b.performer', 'p')
                ->andWhere('p.id = :performer')->setParameter('performer', $filter->getPerformer());
        }

        if ($filter->getGenre()) {
            $qb
                ->join('b.genres', 'g')
                ->andWhere('g.name = :genre')->setParameter('genre', $filter->getGenre());
        }

        $qb
            ->andWhere('b.removed IS NULL')
            ->orderBy('b.biblioId', Criteria::ASC)
            ->addOrderBy('b.id', Criteria::DESC);

        return $qb;
    }

    public function findRandomByGenre($genres, Book $book) {
        return $this->createQueryBuilder('b')
            ->join('b.genres', 'genres')
            ->andWhere('genres.name IN (:genres)')->setParameter('genres', array_map(function (Genre $genre) {
                return $genre->getName();
            }, $genres->getValues()))
            ->andWhere('b.removed IS NULL')
            ->andWhere('b.id <> :bookId')
            ->setParameter('bookId', $book->getId())
//            ->addSelect('RAND() as HIDDEN rand')
//            ->orderBy('rand()')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    public function findSimilar(Book $book) {
        return $this->createQueryBuilder('b')
            ->andWhere('b.title LIKE :title')
            ->andWhere('b.id <> :id')
            ->setParameter('id', $book->getId())
            ->andWhere('b.removed IS NULL')
            ->setParameter('title', '%'.$book->getTitle().'%')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }

    public function findForBiblioSync(int $partCount) {

        return $this->createQueryBuilder('b')
            ->join('b.parts', 'p')
            ->addSelect('COUNT(p) as HIDDEN n')
            ->having('n = :partCount')
            ->setParameter('partCount', $partCount)
            ->groupBy('b.id')
            ->getQuery()
            ->getResult();
    }

}
