<?php

use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__).'/vendor/autoload.php';

if (is_file(\dirname(__DIR__).'/Resources/google_key.json')) {
    putenv('GOOGLE_APPLICATION_CREDENTIALS=' . \dirname(__DIR__).'/Resources/google_key.json');
}
